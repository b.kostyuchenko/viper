VIPER
============================

В данном репозитории лежит шаблон кодогенерации VIPER для Generamba

**Инструкция**
* Устанавливаем генерамбу: **gem install generamba**
* Обновляем шаблоны с помощью: **generamba template install**
* В левом меню с файлами выбираем tagret проекта, затем в правом меню с настройками проекта выбираем Xcode 8.0-compatible
* Для генерации нового модуля достаточно написать generamba gen [MODULE_NAME] viper
* Подробнее тут: https://github.com/rambler-digital-solutions/Generamba!

